import Vue from "vue";
Vue.mixin({
  methods: {
    created() {
      setInterval(() => {
        navigator.serviceWorker.controller.postMessage(
          this.$store.state.cart.cart
        );
      }, 2000);
    }
  }
});
