export const totals = payloadArr => {
  const totalPrice = payloadArr
    .map(cartArr => {
      let qty = parseInt(cartArr.qty);
      return cartArr.price * qty;
    })
    .reduce((a, b) => a + b, 0);

  const totalQuantity = payloadArr
    .map(cartArr => {
      return parseInt(cartArr.qty);
    })
    .reduce((a, b) => a + b, 0);

  return {
    price: totalPrice.toFixed(2),
    qty: totalQuantity
  };
};
export const state = () => {
  return {
    cart: [],
    totalPrice: 0,
    totalQuantity: 0
  };
};
export const mutations = {
  ADD_TO_CART(state, payload) {
    console.log(state.cart);
    state.cart = [...state.cart, ...payload];
  },
  DELETE_IN_CART(state, _id) {
    const currentCartToDelete = state.cart;
    const indexToDelete = currentCartToDelete.findIndex(cart => {
      return cart._id == _id;
    });

    state.cart = [
      ...currentCartToDelete.slice(0, indexToDelete),
      ...currentCartToDelete.slice(indexToDelete + 1)
    ];
    console.log(state.cart);
  },
  UPDATE_CART(state, payload) {
    const currentCartToUpdate = state.cart;
    const changedState = currentCartToUpdate.map(cart => {
      payload.forEach(product => {
        if (cart._id == product._id) {
          return (cart.qty = product.value);
        }
      });
    });
    state.totalPrice = totals(state.cart).price;
    state.totalQuantity = totals(state.cart).qty;
  },
  EMPTY_CART(state) {
    state.cart.length = 0;
    state.totalPrice = 0;
    state.totalQuantity = 0;
  }
};

export const actions = {
  addToCart({ commit }, payload) {
    commit("ADD_TO_CART", payload);
  },
  deleteInCart({ commit }, _id) {
    commit("DELETE_IN_CART", _id);
  },
  updateCart({ commit }, payload) {
    commit("UPDATE_CART", payload);
  },
  emptyCart({ commit }) {
    commit("EMPTY_CART");
  }
};

export const getters = {
  cart(state) {
    return state.cart;
  },
  totalPrice(state) {
    return state.totalPrice;
  },
  totalQuantity(state) {
    return state.totalQuantity;
  }
};
