export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: "universal",
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "server",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: "Lepape: équipement running & trail, vélo, fitness et outdoor",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    script: [
      { src: "/all.js" },
      { src: "/register_service_worker.js" },
      { src: "/fetch.js" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/icon.ico" }]
  },
  /*
   ** Global CSS
   */
  css: ["@/assets/css/style.css"],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    { src: "~/plugins/multiTabState.client.js" },
    { src: "~/plugins/main.js" }
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    //'@nuxtjs/eslint-module'
  ],
  icon: {
    iconSrc: "./static/icon.png"
  },
  manifest: {
    short_name: "Basic PWA",
    name: "Basic Pwa Example",
    start_url: "/",
    theme_color: "#343A40"
  },
  workbox: {
    swDest: "static/sw.js",
    runtimeCaching: [
      {
        urlPattern: "http://localhost:5000/image/.*",
        handler: "cacheFirst",
        method: "GET",
        strategyOptions: {
          cacheName: "images",
          cacheableResponse: { statuses: [0, 200] }
        }
      }
    ]
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    "bootstrap-vue/nuxt",
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    // Doc: https://github.com/nuxt/content
    "@nuxt/content",
    [
      "nuxt-fontawesome",
      {
        imports: [
          {
            set: "@fortawesome/free-solid-svg-icons",
            icons: ["fas"]
          },
          {
            set: "@fortawesome/free-brands-svg-icons",
            icons: ["fab"]
          }
        ]
      }
    ]
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {},
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    transpile: ["idb.js"]
  },
  server: {
    port: 5555
  }
};
