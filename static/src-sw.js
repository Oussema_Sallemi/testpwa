importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);

let items = [
  {
    _id: 123,
    name: "sandwich",
    price: 4.99,
    description: "A very tasty sandwich",
    created: new Date().getTime()
  },
  {
    _id: 124,
    name: "sandwich",
    price: 4.99,
    description: "A very tasty sandwich",
    created: new Date().getTime()
  }
];

const cacheName = "lepape-cache";

function createIndexedDB() {
  var version = 1;
  var db = "lepape"; // database
  var store = "cart"; // store

  // open the database
  var request = indexedDB.open(db, version); // connect + open database
  var db = false;
  // if error
  request.onerror = function(event) {
    console.log(event.target);
    console.trace();
  };

  // if success
  request.onsuccess = function(event) {
    console.log(event.target);
    console.trace();
  };

  // if onupgradeneeded
  request.onupgradeneeded = function(event) {
    console.log(event.target);
    console.trace();
    db = event.target.result;
    var objectStore = db.createObjectStore(store, {
      keyPath: "_id",
      autoIncrement: true
    });
  };
}

const dbPromise = createIndexedDB();

function saveEventDataLocally(apiInfo) {
  let db;
  let request = indexedDB.open("lepape", 1);
  request.onsuccess = function(evt) {
    db = request.result;
    const tx = db.transaction("cart", "readwrite");
    apiInfo.forEach(value => {
      let request = tx.objectStore("cart").add(value);
    });

    return tx.complete;
  };
}

function getLocalEventData() {
  let db;
  let request = indexedDB.open("lepape", 1);
  request.onsuccess = function(evt) {
    db = request.result;
    const tx = db.transaction("cart", "readonly");
    const store = tx.objectStore("cart");
    return console.log(store.getAll());
  };
}

workbox.setConfig({
  debug: true
});

workbox.precaching.cleanupOutdatedCaches();

workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);

workbox.core.clientsClaim();
workbox.core.skipWaiting();

workbox.routing.registerRoute(
  new RegExp("/_nuxt/.*"),
  new workbox.strategies.CacheFirst({}),
  "GET"
);
workbox.routing.registerRoute(
  new RegExp("/.*"),
  new workbox.strategies.NetworkFirst({}),
  "GET"
);
workbox.routing.registerRoute(
  new RegExp("/"),
  new workbox.strategies.StaleWhileRevalidate({}),
  "GET"
);

workbox.routing.registerRoute(
  /.*\.(?:png|jpg|jpeg|svg|gif|webp|bmp|svg)/,
  new workbox.strategies.CacheFirst({
    cacheName: "api-images",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      })
    ]
  }),
  "GET"
);
workbox.routing.registerRoute(
  new RegExp("http://localhost:5000/image/.*"),
  new workbox.strategies.CacheFirst({}),
  "GET"
);

self.addEventListener("message", e => {
  if (e.data) {
    saveEventDataLocally(Object.values(e.data));
  }
});

const cachedFetch = request =>
  request.method != "GET"
    ? fetch(request)
    : caches.open(cacheName).then(cache =>
        cache.match(request).then(resp => {
          if (!!resp) {
            console.log("> from cache", request.url);
            return resp;
          } else {
            console.log("! not in cache", request.url);
            return fetch(request).then(response => {
              cache.put(request, response.clone());
              return response;
            });
          }
        })
      );

self.addEventListener("fetch", event =>
  event.respondWith(cachedFetch(event.request))
);

//const dataAdd = saveEventDataLocally(payload);
//const dataRead = getLocalEventData();
