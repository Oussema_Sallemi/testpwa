module.exports = {
  globDirectory: "static/",
  globPatterns: ["**/*.{js,ico,png,jpg,md}"],
  swDest: "static/sw.js",
  swSrc: "static/src-sw.js"
};
